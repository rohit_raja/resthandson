package com.rohit.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/resttest")
public class RestTest {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/hello")
	public String sayHello(){
		return "Hello from Rohit";
	}
	/*
	 * test
	 */

}
